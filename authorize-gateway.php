<?php
// provides BasicPaymentGateway interface
require 'code-challenge.php';


// i went with authorize.net for the demo
class AuthorizeGateway implements BasicPaymentGateway {

    // being payment related, lets store most data with 'private' visibility
    private $ch;
    private $info = [];
    private $headers = ['Content-Type: Application/JSON'];
    private $last_response = [];
    private $last_txn_id = false;

    // public config and error access
    public $endpoint = "https://apitest.authorize.net/xml/v1/request.api";
    public $errors = [];

    /**
     * @param   string  API login from authorize.net
     * @param   string  API key from authorize.net
     */
    public function __construct($api_login, $api_key) {

        // store our secret key for later
        $this->api_login = $api_login;
        $this->api_key   = $api_key;

        // init curl
        $this->ch = curl_init($this->endpoint);
    }

    /**
     * @param   string  First name on card.
     * @return  object  Chainable instance of self.
     */
    public function setFirstName($name){

        $this->info['first_name'] = $name;

        return $this;
    }

    /**
     * @param   string  Last name on card.
     * @return  object  Chainable instance of self.
     */
    public function setLastName($name){

        $this->info['last_name'] = $name;

        return $this;
    }

    /**
     * @param   string  Billing address, line 1.
     * @return  object  Chainable instance of self.
     */
    public function setAddress1($address){

        $this->info['address1'] = $address;

        return $this;
    }

    /**
     * @param   string  Billing address, line 2.
     * @return  object  Chainable instance of self.
     */
    public function setAddress2($address){

        $this->info['address2'] = $address;

        return $this;
    }

    /**
     * @param   string  Billing city.
     * @return  object  Chainable instance of self.
     */
    public function setCity($city){

        $this->info['city'] = $city;

        return $this;
    }

    /**
     * @param   string  Billing state/province.
     * @return  object  Chainable instance of self.
     */
    public function setProvince($province){

        $this->info['province'] = $province;

        return $this;
    }

    /**
     * @param   string  Billing zip/postal code.
     * @return  object  Chainable instance of self.
     */
    public function setPostal($postal){

        $this->info['postal'] = $postal;

        return $this;
    }

    /**
     * @param   string  Billing country.
     * @return  object  Chainable instance of self.
     */
    public function setCountry($country){

        $this->info['country'] = $country;

        return $this;
    }

    /**
     * @param   string  Card number.
     * @return  object  Chainable instance of self.
     */
    public function setCardNumber($number){

        $this->info['card'] = $number;

        return $this;
    }

    /**
     * @param   string  Card expiration month in MM format.
     * @param   string  Card expiration year in YYYY format.
     * @return  object  Chainable instance of self.
     */
    public function setExpirationDate($month, $year){

        $this->info['exp_month'] = $month;
        $this->info['exp_year']  = $year;

        return $this;
    }

    /**
     * @param   string  Card security code (CVV, CVV2, etc.).
     * @return  object  Chainable instance of self.
     */
    public function setCvv($cvv){

        $this->info['cvv'] = $cvv;

        return $this;
    }

    /**
     * @param string  JSON request created from createTransactionRequest()
     * @return string JSON encoded request
     */
    public function createTransactionRequest($amount, $currency) {

        $fields = [
            'createTransactionRequest' =>
                [
                    'merchantAuthentication' =>
                        [
                            'name' => $this->api_login,
                            'transactionKey' => $this->api_key,
                        ],
                    'transactionRequest' =>
                        [
                            'transactionType' => 'authCaptureTransaction',
                            'amount' => $amount,
                            'currencyCode' => $currency,
                            'payment' =>
                                [
                                    'creditCard' =>
                                        [
                                            'cardNumber' => $this->info['card'],
                                            'expirationDate' => $this->info['exp_year'] . '-' . $this->info['exp_month'],
                                            'cardCode' => $this->info['cvv'],
                                        ],
                                ],
                            'billTo' =>
                                [
                                    'firstName' => $this->info['first_name'],
                                    'lastName' => $this->info['last_name'],
                                    'address' => trim($this->info['address1'] . '\n' . $this->info['address2']),
                                    'city' => $this->info['city'],
                                    'state' => $this->info['province'],
                                    'zip' => $this->info['postal'],
                                    'country' => $this->info['country'],
                                ],
                            'customerIP' => $_SERVER['REMOTE_ADDR'],
                        ],
                ],
        ];

        return json_encode($fields);
    }

    /**
     * @param   string  Error message to be added to array of errors
     * @return  bool    FALSE
     */
    public function setError($err) {
        $this->errors[] = $err;

        return false;
    }

    /**
     * @param   string  '0.00' format.
     * @param   string  [optional] Currency. Default = USD.
     * @return  bool    TRUE if charge successful, FALSE otherwise.
     */
    public function charge($amount, $currency = 'USD') {

        // creates json array to be sent to authorize.net with our transaction request
        $request = $this->createTransactionRequest($amount, $currency);

        // this will set all the necessary curl options
        // called at the time of charge() so we dont accidentally used cached info
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($this->ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($this->ch, CURLOPT_POST, true);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $request);

        // this hits our endpoint
        $cx = curl_exec($this->ch);

        // check for curl errors before api errors
        if($cx === false) {
            return $this->setError("cURL Error: " . curl_error($cx));
        } else {

            // ref: https://stackoverflow.com/questions/35196886/authorize-net-json-return-extra-characters
            // apparently authorize.net, since 2016, has been sending invisible characters with their responses
            // i even complained to my girlfriend about this one
            $cx = preg_replace('/^[\x00-\x1F\x80-\xFF]{3,3}/', '', $cx);

            // decode our response
            $json = json_decode($cx, true);

            // store it
            $this->last_response = $json;

            // check for validity
            if(!isset($json['transactionResponse'])) {
                return $this->setError("The processor returned an invalid response");
            }

            // make life easier
            $txn = $json['transactionResponse'];

            // check for errors
            if(isset($txn['errors'])) {

                // there may be multiple errors
                foreach($txn['errors'] as $err) {
                    $this->setError($err['errorText']);
                }

                // we found errors, transaction is unsuccessful
                return false;
            }

            // store our last transaction id for easy reference
            if(isset($txn['transId'])) {
                $this->last_txn_id = $txn['transId'];
            }

            // check for success
            if(isset($txn['messages']) && isset($txn['messages'][0]) && $txn['messages'][0]['code'] == '1') {
                return true;
            } else {

                // we all hate unknown errors but, better to catch them than encounter them unexpectedly
                $this->setError("An unknown error has occurred");
            }
        }
    }

    /**
     * @return  array  Empty if no errors found.
     */
    public function getErrors(){
        return $this->errors;
    }

    /**
     * @return  string  Transaction ID of the last successful API operation.
     */
    public function getTransactionId(){
        return $this->last_txn_id;
    }

}

?>