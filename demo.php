<?php
// provides AuthorizeGateway class
require 'authorize-gateway.php';

$gw = new AuthorizeGateway('925hDnUuTCZ', '848V7x2Aq4BgFn9F');
$gw->setFirstName('Bob')
   ->setLastName('Smith')
   ->setAddress1('123 Test Street')
   ->setAddress2('Suite #1337')
   ->setCity('Morristown')
   ->setProvince('TN')
   ->setPostal('37814')
   ->setCountry('US')
   ->setCardNumber('4007000000027')
   ->setExpirationDate('10', '2019')
   ->setCvv('123');

if ($gw->charge('49.99', 'USD')) {
   echo "Charge successful! Transaction ID: " . $gw->getTransactionId();
} else {
   echo "Charge failed. Errors: " . print_r($gw->getErrors(), TRUE);
}

?>